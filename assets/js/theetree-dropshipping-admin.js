function sync_products_and_categories() {

    var sync_data = document.getElementById('sync_data');
    var data_syncing_bar = document.getElementById('data_syncing_bar');
    sync_data.classList.toggle('hidden');
    data_syncing_bar.classList.toggle('hidden');

    var data = {
        action: 'sync_category_and_products'
    };

    jQuery.post(ajaxurl, data, function (response) {
        alert('Done: ' + response);
        sync_data.classList.toggle('hidden');
        data_syncing_bar.classList.toggle('hidden');
    });

}