<?php

// New order status AFTER woo 2.2
add_action( 'init', 'register_in_theetree_process_order_statuses' );

function register_in_theetree_process_order_statuses() {
    register_post_status( 'wc-in-tet-process', array(
        'label'                     => _x( 'In TheETree Process', 'Order status', 'theetree-woocommerce-dropshipping' ),
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'In TheETree Process <span class="count">(%s)</span>', 'In TheETree Process<span class="count">(%s)</span>', 'theetree-woocommerce-dropshipping' )
    ) );
}

add_filter( 'wc_order_statuses', 'in_theetree_process_wc_order_statuses' );

// Register in wc_order_statuses.
function in_theetree_process_wc_order_statuses( $order_statuses ) {
    $order_statuses['wc-in-tet-process'] = _x( 'In TheETree Process', 'Order status', 'theetree' );
    return $order_statuses;
}