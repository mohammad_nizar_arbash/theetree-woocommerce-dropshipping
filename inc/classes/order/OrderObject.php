<?php


class OrderObject
{
    public $billing_info; //Billing_info
    public $ship_to_customer; //boolean
    public $address; //Address
    public $ordered_products; //array( Ordered_products )

}

class Billing_info {
    public $name; //String
    public $surname; //String
    public $address; //String
    public $city; //String
    public $zip; //String
    public $country; //String
    public $state; //String
    public $email; //String
    public $phone_number; //String

}
class Address {
    public $name; //String
    public $surname; //String
    public $address; //String
    public $city; //String
    public $zip; //String
    public $country; //String
    public $state; //String
    public $email; //String
    public $phone_number; //String

}
class Options {
    public $optionId; //int
    public $optionName; //String
    public $valueId; //int
    public $ValueName; //String
}
class Ordered_products {
    public $product_id; //int
    public $options; //array( Options )
    public $qty; //int
    public $quantity; //int

}
