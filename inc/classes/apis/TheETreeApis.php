<?php

if (!class_exists('TheETreeApis')) {
    class TheETreeApis
    {

        private static function auth()
        {
            $key = TheETreeHelper::get_consumer_key();
            $secret = TheETreeHelper::get_consumer_secret();

            $remote_url = THEETREE_SERVER_API_URL . 'auth/login';

            $body = [
                'consumer_key' => $key,
                'consumer_secret_key' => $secret,
                'is_api' => true,
            ];

            $args = array(
                'method' => 'POST',
                'body' => $body,
                'Accept' => 'application/json',
                'timeout' => 120,
                'headers' => array(
                    'Content-type: application/x-www-form-urlencoded'
                ),
                'sslverify' => false,
                'httpversion' => '1.0',
                'blocking' => true,
            );

            $result = wp_remote_request($remote_url, $args);

            $status = wp_remote_retrieve_response_code($result);
            if ($status == 200) {
                $body_result = wp_remote_retrieve_body($result);

                $json_decoded = json_decode($body_result, true);

                $access_token = $json_decoded['data']['access_token'];

                TheETreeHelper::set_access_token($access_token);
                return $access_token;


            }
            return null;
        }

        private static function theetreeApiRequest($api_url, $method, $body = null)
        {
            $remote_url = THEETREE_SERVER_API_URL . $api_url;
            if(THEETREE_DEBUG_IN_URL){
                $remote_url .= '?XDEBUG_SESSION_START=PHPSTORM';
            }
            $id_token = TheETreeHelper::get_access_token();
            if (is_null($id_token)) {
                $id_token = self::auth();
            } else {
                if (!self::tokenIsValid()) {
                    $id_token = self::auth();
                }
            }

            if (!is_null($id_token)) {


                $args = array(
                    'method' => $method,
                    'headers' => array(
                        'Content-type'=> 'application/json; charset=utf-8',
                        'Authorization' => 'Bearer ' . $id_token,
                        'Accept'=> 'application/json',
                    ),
                    'body' => $body,
                    'Accept' => 'application/json',
                    'timeout' => 10000,
                    'sslverify' => false,
                    'httpversion' => '1.0',
                    'blocking' => true,
                    'data_format' => 'body',
                );

                $result = wp_remote_request($remote_url, $args);
                $status = wp_remote_retrieve_response_code($result);

                if ($status == 200 || $status == 520) {

                    $body_result = wp_remote_retrieve_body($result);
                    $json_decoded = json_decode($body_result, true);

                    $json_decoded['response_status'] = $status;

                    return $json_decoded;
                }
            }
            return null;
        }

        private static function tokenIsValid()
        {
            $remote_url = THEETREE_SERVER_API_URL . 'auth/jwtCheck';
            $id_token = TheETreeHelper::get_access_token();

            $args = array(
                'method' => 'GET',
                'headers' => array(
                    'Authorization' => 'Bearer ' . $id_token,
                ),
                'Accept' => 'application/json',
                'timeout' => 60
            );

            $result = wp_remote_request($remote_url, $args);
            if (wp_remote_retrieve_response_code($result) == 200) {
                return true;
            } else {
                return false;
            }

        }

        public static function getUserInformation()
        {
            $url = 'auth/me';

            return self::theetreeApiRequest($url, 'GET');
        }

        public static function getStoreInformation()
        {
            $url = 'storeapis/getStore';


            return self::theetreeApiRequest($url, 'GET');
        }

        public static function getStoreProducts()
        {
            $url = 'storeapis/getProducts';
            $products = self::theetreeApiRequest($url, 'GET');
            return $products['products']['products'];
        }


        public static function getStoreProductsCsv()
        {
            $url = 'storeapis/getProductsCsvFile';

            $remote_url = THEETREE_SERVER_API_URL . $url;
            $id_token = TheETreeHelper::get_access_token();
            if (is_null($id_token)) {
                $id_token = self::auth();
            } else {
                if (!self::tokenIsValid()) {
                    $id_token = self::auth();
                }
            }
            if (!is_null($id_token)) {


                $args = array(
                    'method' => 'GET',
                    'headers' => array(
                        'Authorization' => 'Bearer ' . $id_token,
                    ),
                    'body' => null,
                    'Accept' => 'text/csv',
                    'timeout' => 10000

                );

                $result = wp_remote_request($remote_url, $args);

                $mirror = TheETreeHelper::save_csv_file('product-' . rand() . '.csv', '', wp_remote_retrieve_body($result));

                if (isset($mirror['file'])) {

                    return $mirror['file'];
                }
            }
            return null;
        }

        public static function getStoreCategories()
        {
            $url = 'storeapis/getCategories';

            $categories = self::theetreeApiRequest($url, 'GET');
            return $categories['categories'];
        }

        public static function addOrder($order)
        {
            $url = 'orders_store/create';

            return self::theetreeApiRequest($url, 'POST', $order);
        }


    }
}
