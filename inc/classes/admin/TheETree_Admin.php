<?php


function theetree_admin_enqueue_admin_script( ) {

    wp_enqueue_script( 'theetree-js', THEETREE_PLUGIN_URL. 'assets/js/theetree-dropshipping-admin.js', array(), '1.0' );
}


if(! class_exists('TheETree_Admin')){
    class TheETree_Admin
    {

        public function __construct()
        {
            add_action( 'admin_enqueue_scripts', 'theetree_admin_enqueue_admin_script' );


            add_action( 'admin_menu', array( $this, 'add_menu_pages') );
            add_action( 'admin_init', array( $this, 'handle_theetree_settings_submit' ), 99 );


        }



        public function add_menu_pages() {

            add_menu_page(__( 'TheETree Dropshipping', 'theetree-woocommerce-dropshipping' ),  __( 'Dropshipping', 'theetree-woocommerce-dropshipping' ), 'manage_options', 'theetree_drop',array( $this, 'admin_dropshipping' ),THEETREE_PLUGIN_URL. 'assets/images/theetree.png', '30');
            add_submenu_page( 'theetree_drop', __( 'TheETree Dropshipping', 'theetree-woocommerce-dropshipping' ), __( 'Settings', 'theetree-woocommerce-dropshipping' ),
                'manage_options', 'theetree_settings',array( $this, 'admin_settings' ));

        }


        public function admin_dropshipping(){

            include THEETREE_PLUGIN_DIR.'inc/layouts/dashboard.php';

        }

        public function admin_settings(){

            include THEETREE_PLUGIN_DIR.'inc/layouts/dashboard-setting.php';
        }

        public function handle_theetree_settings_submit(){
            if(isset($_POST['theetree_setting_page'])){
                check_admin_referer( 'theetree_settings_wp_nonce_field', 'theetree_settings_wp_nonce_field' );
                $options = TheETreeHelper::theetree_get_options();
                $new_options = $_POST['theetree_options'];
                if( isset( $new_options['consumer_key'] ) ){
                    $options['consumer_key'] = sanitize_text_field( $new_options['consumer_key'] );
                }

                if( isset( $new_options['consumer_secret'] ) ){
                    $options['consumer_secret'] = sanitize_text_field( $new_options['consumer_secret'] );
                }

                if( isset( $new_options['update_rate'] ) ){
                    $options['update_rate'] = sanitize_text_field( $new_options['update_rate'] );
                }
                $options['access_token'] = null;

                TheETreeHelper::theetree_update_options($options);

                TheeTreeSchedules::clear_theetree_schedules();
                TheeTreeSchedules::start_cron_job();
            }
        }
    }
}
