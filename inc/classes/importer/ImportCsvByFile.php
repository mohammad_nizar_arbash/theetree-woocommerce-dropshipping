<?php


class ImportCsvByFile
{

    protected $importer_csv_class = null;

    public function __construct($file)
    {

        $default_args = array(
            'start_pos'        => 0, // File pointer start.
            'end_pos'          => -1, // File pointer end.
            'lines'            => -1, // Max lines to read.
            'mapping'          => $this->mapping(), // Column mapping. csv_heading => schema_heading.
            'parse'            => true, // Whether to sanitize and format data.
            'update_existing'  => true, // Whether to update existing items.
            'delimiter'        => ',', // CSV delimiter.
            'prevent_timeouts' => false, // Check memory and time usage and abort if reaching limit.
            'enclosure'        => '"', // The character used to wrap text in the CSV.
            'escape'           => "\0", // PHP uses '\' as the default escape character. This is not RFC-4180 compliant. This disables the escape character.
        );



        $this->importer_csv_class = new TheETreeImporterToWC($file, $default_args);

    }

    public function import(){
       $newData = $this->importer_csv_class->import();
     //  $updateData = $this->importer_csv_class_update->import();

       // $log['imported'] = count($newData['imported']);
       // $log['failed'] = count($newData['failed']) + count($updateData['failed']);
       // $log['updated'] = count($newData['updated']);

        return $newData;
    }
    private function mapping(){
        return array (
            'ID' => 'id',
            'Type' => 'type',
            'SKU' => 'sku',
            'Name' => 'name',
            'Language' => 'language',
            'Translation group' => 'translations',
            'Published' => 'published',
            'Is featured?' => 'featured',
            'Visibility in catalog' => 'catalog_visibility',
            'Short description' => 'short_description',
            'Description' => 'description',
            'Date sale price starts' => 'date_on_sale_from',
            'Date sale price ends' => 'date_on_sale_to',
            'Tax status' => 'tax_status',
            'Tax class' => 'tax_class',
            'In stock?' => 'stock_status',
            'Stock' => 'stock_quantity',
            'Low stock amount' => 'low_stock_amount',
            'Backorders allowed?' => 'backorders',
            'Sold individually?' => 'sold_individually',
            'Weight (kg)' => 'weight',
            'Length (cm)' => 'length',
            'Width (cm)' => 'width',
            'Height (cm)' => 'height',
            'Allow customer reviews?' => 'reviews_allowed',
            'Purchase note' => 'purchase_note',
            'Sale price' => 'sale_price',
            'Regular price' => 'regular_price',
            'Categories' => 'category_ids',
            'Tags' => 'tag_ids',
            'Shipping class' => 'shipping_class_id',
            'Images' => 'images',
            'Download limit' => 'download_limit',
            'Download expiry days' => 'download_expiry',
            'Parent' => 'parent_id',
            'Grouped products' => 'grouped_products',
            'Upsells' => 'upsell_ids',
            'Cross-sells' => 'cross_sell_ids',
            'External URL' => 'product_url',
            'Button text' => 'button_text',
            'Position' => 'menu_order',
            'Attribute 1 name' => 'attributes:name1',
            'Attribute 1 value(s)' => 'attributes:value1',
            'Attribute 1 visible' => 'attributes:visible1',
            'Attribute 1 global' => 'attributes:taxonomy1',
            'Attribute 2 name' => 'attributes:name2',
            'Attribute 2 value(s)' => 'attributes:value2',
            'Attribute 2 visible' => 'attributes:visible2',
            'Attribute 2 global' => 'attributes:taxonomy2',
            'Attribute 3 name' => 'attributes:name3',
            'Attribute 3 value(s)' => 'attributes:value3',
            'Attribute 3 visible' => 'attributes:visible3',
            'Attribute 3 global' => 'attributes:taxonomy3',
            'Meta: the_e_tree_metadata' => 'meta:the_e_tree_metadata',
        );
    }
}