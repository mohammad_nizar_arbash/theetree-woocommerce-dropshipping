<?php

if (!defined('ABSPATH')) exit;

$token = TheETreeHelper::get_access_token();

$log_list = new Log_Table();
$log_list->prepare_items();
$next_exc_time = TheeTreeSchedules::get_next_time();
?>

<div class="wrap">

    <div id="icon-users" class="icon32"><br/></div>

    <h3><?php esc_attr_e('TheETree DropShipping', 'theetree-woocommerce-dropshipping'); ?></h3>
    <table class="form-table">
        <tbody>
        <?php
        if (!is_null($token)) {

            $store = TheETreeApis::getStoreInformation();
            //   $store = null;
            if (!is_null($store)) {
                ?>
                <tr>
                    <div id="sync_data" style="height: 75px">
                        <a class="page-title-action" onclick="sync_products_and_categories();"><?php esc_attr_e('Sync Data','theetree-woocommerce-dropshipping'); ?></a>
                    </div>

                    <div id="data_syncing_bar" class="hidden" style="width: 100%; height: 150px">
                        <img src="<?php echo THEETREE_PLUGIN_URL. 'assets/images/progressbar3.gif'; ?>" id="data_syncing_progress_bar"
                             style="height: 70px; width: 100%">
                        <h3>
                            <?php _e('The syncing process may take a few minutes, keep this page open', 'theetree-woocommerce-dropshipping');?>
                        </h3>
                    </div>
                </tr>


                <tr>
                    <div>
                        <?php _e('Connected Store Name : ', 'theetree-woocommerce-dropshipping');
                        echo $store['store']['name']; ?>
                    </div>
                </tr>


                <tr>
                    <div>
                        <?php _e('Next Schedule Sync Time:', 'theetree-woocommerce-dropshipping')." ".$next_exc_time; ?>

                    </div>
                </tr>
                <?php echo $next_exc_time;?>



            <?php } else { ?>
                <td>
                    <?php _e('Please Enter Or Check  Consumer Key And Consumer Secret In Settings Page', 'theetree-woocommerce-dropshipping'); ?>
                </td>

            <?php }
        } ?>

        </tbody>
    </table>

    <!-- Forms are NOT created automatically, so you need to wrap the table in one to use features like bulk actions -->
    <form id="movies-filter" method="get">
        <!-- For plugins, we also need to ensure that the form posts back to our current page -->
        <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
        <!-- Now we can render the completed list table -->
        <?php $log_list->display() ?>
    </form>

</div>
