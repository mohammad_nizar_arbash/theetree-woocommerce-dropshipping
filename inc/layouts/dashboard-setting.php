<?php

if (!defined('ABSPATH')) exit;

$theetree_options = TheETreeHelper::theetree_get_options();
$consumer_key = isset($theetree_options['consumer_key']) ? esc_attr($theetree_options['consumer_key']) : '';
$consumer_secret = isset($theetree_options['consumer_secret']) ? esc_attr($theetree_options['consumer_secret']) : '';
$update_rate = isset($theetree_options['update_rate']) ? esc_attr($theetree_options['update_rate']) : 12;

$token = TheETreeHelper::get_access_token();

?>
<div>

    <h3><?php esc_attr_e('TheETree DropShipping - Settings', 'theetree-woocommerce-dropshipping'); ?></h3>
    <form method="post">
        <table class="form-table">
            <tbody>

            <tr>
                <th scope="row">
                    <?php _e('TheETree Consumer Key', 'theetree-woocommerce-dropshipping'); ?>
                </th>
                <td>
                    <input name="theetree_options[consumer_key]" type="text" value="<?php if ($consumer_key != '') {
                        echo $consumer_key;
                    } ?>"/>
                    <p class="description">


                    </p>
                </td>
            </tr>

            <tr>
                <th scope="row">
                    <?php _e('TheETree Consumer Secret', 'theetree-woocommerce-dropshipping'); ?>
                </th>
                <td>
                    <input name="theetree_options[consumer_secret]" type="text"
                           value="<?php if ($consumer_secret != '') {
                               echo $consumer_secret;
                           } ?>"/>
                    <p class="description">

                    </p>
                </td>
            </tr>

            <tr>
                <th scope="row">
                    <?php _e('Update Rate', 'theetree-woocommerce-dropshipping'); ?>

                </th>
                <td>
                    <select name="theetree_options[update_rate]">
                        <?php
                        $times = TheeTreeSchedules::$theetree_supported_times;
                        foreach ($times as $time=>$value) {
                            ?>

                            <option value="<?php echo $time;?>" <?php if ($update_rate == $time) echo 'selected'; ?>>
                                <?php echo __($time,'theetree-woocommerce-dropshipping')?></option>

                        <?php } ?>

                    </select>
                </td>
            </tr>

            </tbody>
        </table>
        <div>
            <?php wp_nonce_field('theetree_settings_wp_nonce_field', 'theetree_settings_wp_nonce_field'); ?>
            <input type="hidden" name="theetree_setting_page" value="theetree_setting_page">
            <input type="submit" value="<?php esc_attr_e('Save Settings', 'theetree-woocommerce-dropshipping'); ?>"/>
        </div>

        <?php
        if (!is_null($token)) {

            $store = TheETreeApis::getStoreInformation();
            if (!is_null($store)) {
                ?>
                <div>
                    <?php _e('Connected Store Name :', 'theetree-woocommerce-dropshipping');
                    echo $store['store']['name']; ?>
                </div>
            <?php } else { ?>
                <?php _e('The Consumer Key And Secret Not Valid', 'theetree-woocommerce-dropshipping'); ?>
            <?php }
        } ?>

    </form>
</div>