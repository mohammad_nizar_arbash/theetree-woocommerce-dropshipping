<?php

if (!function_exists('sync_category_and_products_callback')) {

    function sync_category_and_products_callback()
    {
        CornLog::tet_add_to_log(0,0,0,0,'Start Manual');

        $products_status = ImportHelper::sync_products();

        CornLog::tet_add_to_log(count($products_status['updated']), count($products_status['imported']),
            count($products_status['skipped']),count($products_status['failed']),'Manual');

        echo __("Updated Products: ",'theetree-woocommerce-dropshipping').count($products_status['updated'])
            . __(", Imported Products: ",'theetree-woocommerce-dropshipping').count($products_status['imported']).
            __(", Skipped Products: ",'theetree-woocommerce-dropshipping').count($products_status['skipped']).
            __(", Failed Products: ",'theetree-woocommerce-dropshipping') .count($products_status['failed']);
    }

}

add_action('wp_ajax_sync_category_and_products', 'sync_category_and_products_callback');


