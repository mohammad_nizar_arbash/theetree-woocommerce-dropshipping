<?php

if (!function_exists('add_new_order_callback')) {

    function add_new_order_callback()
    {

        if (isset($_GET['order_id'])) {
            if (check_if_order_exist($_GET['order_id'])) {
                echo "This Order has already been added ";
                add_flash_notice(__("TheETree Add New Order, This order is already added",'theetree-woocommerce-dropshipping'), "warning", false);

            } else {

                $order = get_order_wc_to_tet($_GET['order_id']);
                if (!is_null($order)) {
                    if ($order['is_done']) {
                        add_flash_notice(__("TheETree Add New Order, This order is added Successfully with order id: ",'theetree-woocommerce-dropshipping') . $order['tet_order_uuid'],
                            "info", false);

                    } else {
                        add_flash_notice(__("TheETree Add New Order, Error Message : ",'theetree-woocommerce-dropshipping') . $order['message'],
                            "error", false);
                    }

                } else {
                    add_flash_notice(__("TheETree Add New Order, An error occurred while adding your order, try later, or enter it manually using the addition of a new order within TheETree platform",'theetree-woocommerce-dropshipping'),
                        "error", false);
                }

                if (!is_null($order['errors']) && count($order['errors']) > 0) {

                    $error_string = 'Errors : ';
                    foreach ($order['errors'] as $index => $error) {
                        $error_string .= ($index + 1) . '- ' . $error;
                    }

                    add_flash_notice(__("TheETree Add New Order, ",'theetree-woocommerce-dropshipping') . $error_string,
                        "error", false);
                }
            }
        }

        wp_redirect($_SERVER['HTTP_REFERER']);

    }

}


add_action('admin_action_tet_add_new_order', 'add_new_order_callback');


function get_order_wc_to_tet($order_id)
{
    $errors = [];
    $order = new WC_Order($order_id);

    $order_data = $order->get_data(); // The Order data


## BILLING INFORMATION:

    $billing_info = new Billing_info();
    $billing_info->name = $order_data['billing']['first_name'];
    $billing_info->surname = $order_data['billing']['last_name'];

    $billing_info->address = $order_data['billing']['address_1'] . " " . $order_data['billing']['address_2'];

    $billing_info->city = $order_data['billing']['city'];
    $billing_info->state = $order_data['billing']['state'];
    $billing_info->zip = $order_data['billing']['postcode'];
    $billing_info->country = $order_data['billing']['country'];
    $billing_info->email = $order_data['billing']['email'];
    $billing_info->phone_number = $order_data['billing']['phone'];

## SHIPPING INFORMATION:

    $shipping_info = new Address();

    $shipping_info->name = $order_data['shipping']['first_name'];
    $shipping_info->surname = $order_data['shipping']['last_name'];
    $shipping_info->address = $order_data['shipping']['address_1'] . " " . $order_data['shipping']['address_2'];
    $shipping_info->city = $order_data['shipping']['city'];
    $shipping_info->state = $order_data['shipping']['state'];
    $shipping_info->zip = $order_data['shipping']['postcode'];
    $shipping_info->country = $order_data['shipping']['country'];
    $shipping_info->phone_number = $order_data['billing']['phone'];
    $shipping_info->email = $order_data['billing']['email'];


    $order_products = [];

    foreach ($order->get_items() as $item_key => $item):

        ## Using WC_Order_Item methods ##

        // Item ID is directly accessible from the $item_key in the foreach loop or
        $item_id = $item->get_id();

        ## Using WC_Order_Item_Product methods ##

        $product = $item->get_product(); // Get the WC_Product object

        if (!$product) {
            array_push($errors, 'product  not found');
            continue;
        }
        $product_id = $item->get_product_id(); // the Product id

        $variation_attributes = [];
        if ($product->is_type('variation')) {
            $temp = $product->get_attribute_summary();

            $first_split = explode(',', $temp);
            foreach ($first_split as $i => $str) {
                $secound_split = explode(':', $str);
                $variation_attributes[$i]['option_name'] = $secound_split[0];
                $variation_attributes[$i]['value_name'] = $secound_split[1];
            }

        }

        $theetree_options_metadata = get_post_meta($product_id, 'the_e_tree_metadata');


        $theetree_options_metadata_decoded = [];
        if (is_null($theetree_options_metadata[0])) {
            array_push($errors, 'product with id: (' . $product_id . ') not from theetree');
            continue;
        } else {
            $theetree_options_metadata_decoded = json_decode($theetree_options_metadata[0]);
        }

        if (is_null($theetree_options_metadata_decoded) || !isset($theetree_options_metadata_decoded->products_id)) {
            array_push($errors, 'product with id: (' . $product_id . ') not from theetree');
            continue;
        }

        $options_ids = null;
        if (!is_null($variation_attributes) && !is_null($theetree_options_metadata_decoded)) {
            foreach ($variation_attributes as $v)
                $options_ids[] = getOption($theetree_options_metadata_decoded, $v['option_name'], $v['value_name']);
        }

        $quantity = $item->get_quantity();

        $order_product = new Ordered_products();
        $order_product->product_id = $theetree_options_metadata_decoded->products_id;
        $order_product->qty = $quantity;
        $order_product->quantity = $quantity;
        $order_product->options = $options_ids;

        array_push($order_products, $order_product);


    endforeach;

    if (is_null($order_products) || empty($order_products)) {
        if (!empty($errors)) {
            $error_string = 'Errors : ';
            foreach ($errors as $index => $error) {
                $error_string .= ($index + 1) . '- ' . $error;
            }
            return array('is_done' => false, 'message' => $error_string,
                'tet_order_id' => null, 'tet_order_uuid' => null);
        } else {
            return array('is_done' => false, 'message' => __('You dont have any product in this order','theetree-woocommerce-dropshipping'),
                'tet_order_id' => null, 'tet_order_uuid' => null);
        }
    }

    $order_tet_object = new OrderObject();
    $order_tet_object->address = $shipping_info;
    $order_tet_object->billing_info = $billing_info;
    $order_tet_object->ship_to_customer = true;
    $order_tet_object->ordered_products = $order_products;


    $result = OrderHelper::tet_add_order($order_tet_object, $order_id);
    $result['errors'] = $errors;

    if($result['is_done']){
        $order->add_order_note( __('the order add to theetree dropshipping platform with id: ','theetree-woocommerce-dropshipping')
            .$result['tet_order_uuid'] );
        $order->update_status('wc-in-tet-process');
    }
    return $result;
}

function getOption($theetree_options, $option_name, $option_value)
{
    $option = new Options();
    foreach ($theetree_options->options as $data) {

        if (trim($data->option_name) == trim($option_name)) {
            $option->optionId = $data->option_id;
            $option->optionName = trim($option_name);
            foreach ($data->values as $value) {
                $v_name = trim($value->value_name);
                if ($v_name == trim($option_value)) {
                    $option->valueId = $value->value_id;
                    $option->ValueName = trim($option_value);
                    break;
                }
            }
            break;
        }
    }
    return $option;
}